export class ResultPage {
    
    navigate() {
        cy.visit('http://127.0.0.1:8000/polls/1/results/')
    }

    countInitialVotes(votes) {
        cy.get('ul>li').each(($el, index, list) => {
            cy.wrap($el).invoke('text').then((text)=>{ 
                let option = text.split('-')[0].trim()
                let actualVotes = text.match(/[0-9]+/g);
                console.log(actualVotes);
                votes[option] = actualVotes 
               })
        })
    }

    goToVote() {
        cy.contains('Vote again?').click()
    }

    validateVoteCountIncrease(option, votes) {
        let newResults = `${option} -- ${Number(votes[option]) + 1} votes`
        cy.contains(option).should('have.text', newResults)
    }
}
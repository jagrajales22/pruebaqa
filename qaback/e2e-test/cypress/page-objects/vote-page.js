export class VotePage {

    voteForOption(option) {
        cy.contains(option).click()
    }

    confirmVote() {
        cy.get('[type="submit"]').click()
    }
}
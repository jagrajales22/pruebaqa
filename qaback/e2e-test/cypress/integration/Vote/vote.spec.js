/// <reference types="cypress" />

import {ResultPage} from "../../page-objects/result-page";
import { VotePage } from "../../page-objects/vote-page";

const resultPage = new ResultPage()
const votePage = new VotePage()
var votes = {};

Given('I open vote result page', () => {
    resultPage.navigate()
})

And('I count initial votes', () => {
    resultPage.countInitialVotes(votes)
})

When('I open vote initial page', () => {
    resultPage.goToVote()
})

And('I vote for {string} option', (option) => {
    votePage.voteForOption(option)
})

And('I confirm my vote', () => {
    votePage.confirmVote()
})

Then('The counter for {string} increases', (option) => {
    resultPage.validateVoteCountIncrease(option, votes)
})
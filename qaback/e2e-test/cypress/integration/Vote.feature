Feature: Vote

    I want to vote for a selected option

    @smoke
    Scenario Outline: Vote
        Given I open vote result page
        And I count initial votes
        When I open vote initial page
        And I vote for "<option>" option
        And I confirm my vote
        Then The counter for "<option>" increases 

        Examples:
            |   option      |
            |   Not much    |
            |   The sky     | 
            |   La la la    | 